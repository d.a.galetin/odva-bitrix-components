# Bitrix Components

- [Список элементов](/odva/elements/)
- [Детальная элемента](/odva/element/)
- [Список разделов](/odva/sections/)
- [Детальная раздела](/odva/section/)
- [Список заказов](/odva/orders/)
- [Личный кабинет](/odva/profile/)
